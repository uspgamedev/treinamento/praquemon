class_name Item
extends CharacterBody2D
## Items in the game that can be pushed or picked up by the player

@export var item_type: String = ""
@export var item_sprite: Texture2D
@export var pickable: bool = true
@export var pushable: bool = true

var player_in_area: bool = false

@onready var inventory: Inventory = get_tree().get_first_node_in_group("INVENTORY")


## Sets the item sprite to the one defined in the editor.
func _ready():
	$Sprite2D.texture = item_sprite


## Moves the [Item] when pushed, with the same speed as the pusher, along the collision normal.
func push(collision_normal, pusher_velocity):
	velocity = pusher_velocity.dot(collision_normal) * collision_normal
	
	move_and_slide()


## Detects when the [Player] enter the [Item]'s interaction area.
func _on_area_2d_body_entered(body):
	if body is Player: player_in_area = true


## Detects when the [Player] leaves the [Item]'s interaction area.
func _on_area_2d_body_exited(body):
	if body is Player: player_in_area = false


## Detects input to pick up [Item] while the [Player] is inside it's interaction area.
func _input(event):
	if player_in_area and event.is_action_pressed("interact"):
		inventory.pick_item(self.duplicate())
		queue_free()
