class_name InventorySlot
extends VBoxContainer

const SELECTED_TEXTURE : Texture2D = preload("res://assets/old_sprites/beautiful_selected_slot.png")

var item: Object = null
var selected_panel: StyleBoxTexture = StyleBoxTexture.new()

@onready var inventory: Inventory = get_tree().get_first_node_in_group("INVENTORY")


## Fills [InventorySlot] with given [Item].
func fill(new_item: Item):
	item = new_item
	
	$PanelContainer/MarginContainer/ItemSprite.texture = new_item.item_sprite
	$ItemName.text = new_item.item_type


## Empties out the [InventorySlot].
func empty():
	item = null
	
	$PanelContainer/MarginContainer/ItemSprite.texture = null
	$ItemName.text = "-"


func select():
	selected_panel.set_texture(SELECTED_TEXTURE)
	$PanelContainer.add_theme_stylebox_override("panel", selected_panel)
