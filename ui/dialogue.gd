extends Control

var dialogue : Array[String] = []
var speaker : String
var dialogue_active = false

@export var button_scene : PackedScene

@onready var dialogue_text = %DialogueText
@onready var speaker_name = %SpeakerName


# Called when the node enters the scene tree for the first time.
func _ready():
	visible = false


func _process(delta):
	continue_dialogue()


func show_dialogue(speaker, text : Array[String]):
	dialogue = text.duplicate()
	speaker_name.set_text(
		"[b][color=#" + speaker.character_color.to_html() + "]" + speaker.character_name + "[/color][/b]: "
		)
	visible = true
	

func choice(speaker, question : String, answers : Array[String]):
	dialogue = [question]
	speaker_name.set_text(
		"[b][color=#" + speaker.character_color.to_html() + "]" + speaker.character_name + "[/color][/b]: "
		)
	for answer in answers.size():
		var answer_button = button_scene.instantiate()
		answer_button.text = answers[answer]
		answer_button.visible = true
		answer_button.pressed.connect(_on_choice_pressed.bind(answer))
		%HBoxContainer2.add_child(answer_button)
	visible = true
	dialogue_text.set_text(question)

func hide_dialogue():
	visible = false

func continue_dialogue():
	if Input.is_action_just_pressed("interact") and visible:
		var new_dialogue = dialogue.pop_front()
		print(new_dialogue, dialogue)
		if new_dialogue == null:
			hide_dialogue()
			get_tree().set_group("CHARACTER", "is_dialogue_active", false)
			print("Glória a deus saí daqui")
		else:
			dialogue_text.set_text(new_dialogue)
			print("Puta merda ainda tá rolando")


signal answer(index)
func _on_choice_pressed(index):
	answer.emit(index)
	for child in %HBoxContainer2.get_children():
		child.queue_free()
