class_name Inventory
extends Control

const INVENTORY_SLOT: PackedScene = preload("res://ui/inventory_slot.tscn")

var available_slots = 8
var slot_list: Array = []
var selected_index: int = 0

@onready var player = get_tree().get_first_node_in_group("PLAYER")
@onready var slot_grid = $MarginContainer/VBoxContainer/GridContainer


## Initializes [Inventory] with an [available_slots] amount of empty [InventorySlot]s.
func _ready():
	visible = false
	
	for slot_index in range(available_slots):
		var new_slot = INVENTORY_SLOT.instantiate()
		slot_grid.add_child(new_slot)
	
	slot_list = slot_grid.get_children()
	slot_list[0].select()


## Fills first available [InventorySlot] in the [Inventory] with given [Item].
func pick_item(new_item: Item):
	var slot = get_first_empty_slot()
	if slot: slot.fill(new_item)


## Drops [Item] from given [InventorySlot] on the floor near the [Player].
func drop_item(slot: InventorySlot):
	if slot.item:
		var item_collision = slot.item.get_node("CollisionShape2D").shape.get_rect().size
		var player_collision = player.get_node("CollisionShape2D").shape.get_rect().size
		
		slot.item.position = player.position + player_collision / 2 + (item_collision * slot.item.scale)
		get_tree().get_root().add_child(slot.item)
		
		slot.empty()


func use_item(item_index):
	pass


func give_item(item_index):
	pass


## Returns the first empty [InventorySlot] in the [Inventory] or [null] if there is none.
func get_first_empty_slot() -> InventorySlot:
	for slot in slot_list:
		if not slot.item:
			return slot
	
	print("Tá cheio já!!!")
	return null


func switch_selection(key):
	var columns: int = slot_grid.columns
	var new_selected_index: int = 0
	var displacement: int = 0
	
	if key.is_action("ui_left"): displacement = - 1
	elif key.is_action("ui_right"): displacement = 1
	elif key.is_action("ui_up"): displacement = - columns
	elif key.is_action("ui_down"): displacement = columns
	
	new_selected_index = selected_index + displacement
	if new_selected_index < 0: new_selected_index = 0
	if new_selected_index > len(slot_list) - 1: new_selected_index = len(slot_list) - 1
	
	slot_list[selected_index].get_node("PanelContainer").remove_theme_stylebox_override("panel")
	slot_list[new_selected_index].select()
	selected_index = new_selected_index


## Toggle [Inventory] visibility when pressing the designated inventory button.
func _input(event):
	if event.is_action_pressed("inventory"):
		set_visible(!visible)
	elif visible:
		if event.is_action_pressed("interact"):
			drop_item(slot_list[selected_index])
		elif visible and event.is_pressed():
			switch_selection(event)

