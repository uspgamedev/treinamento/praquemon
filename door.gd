class_name Door
extends Area2D

## [Door] the [Player] will be teleported to when entering this [Door]
@export var destination : Door
## Wether the door can be entered through
@export var entrable : bool = true
## Wether the door can be exited through
@export var exitable : bool = true


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


## Teleports [Player] to a position near the destination [Door] defined in the editor
func _on_body_entered(body):
	if body is Player:
		body.position = destination.get_node("ExitPosition").global_position
