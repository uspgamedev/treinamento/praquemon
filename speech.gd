extends Node

@export var character_speech : Array[String] = ["Eaí amigooo!!!", "EI!", "Eaí amigaaa!!!"]
@onready var character = get_parent()
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func interact(speaker):
	var dialogue_node = get_tree().get_first_node_in_group("DIALOGUE")
	dialogue_node.dialogue = character_speech
	dialogue_node.show_dialogue(character, character_speech)
	print("VSF")
