class_name Player
extends CharacterBody2D
## The player and main character of the game


## Intensity of player movement.
const SPEED: float = 300.0

## Diection in which the player will move.
var direction: Vector2 = Vector2(0,0) 

@onready var dialogue = get_tree().get_first_node_in_group("DIALOGUE")
@onready var inventory = get_tree().get_first_node_in_group("INVENTORY")


## Updates character movement every frame, taking into consideration the user input and collisions.
func _physics_process(delta):	
	if dialogue.visible or inventory.visible:
		velocity = Vector2(0,0)
		$AnimatedSprite2D.stop()
	else:
		direction = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
		velocity = direction.normalized() * SPEED

		var collision = KinematicCollision2D.new()
		test_move(transform, velocity * delta * 1.1, collision)
		
		var collider = collision.get_collider()
		if collider is Item and collider.pushable:
			collider.push(collision.get_normal(), velocity)
		
		move_and_slide()
		update_animation(velocity)


## Updates the characters sprite animation according to the direction in which it's moving.
func update_animation(velocity):
	var direction_name: String = ""
	var state: String = ""
	var current_direction_name = $AnimatedSprite2D.animation.split("_")[1]
	
	if velocity == Vector2(0,0):
		state = "idle"
		direction_name = current_direction_name
	else:
		state = "walk"
		if velocity.y > 0: direction_name = "front"
		elif velocity.y < 0: direction_name = "back"
		elif velocity.x > 0: direction_name = "right"
		elif velocity.x < 0: direction_name = "left"
		
	$AnimatedSprite2D.play(state + "_" + direction_name)
	
