extends Node

@export var question : String = "Question?"
@export var answers : Array[String] = ["Answer 1", "Answer 2"]
@export var state_change : Array[Node] = []
@onready var character = get_parent()
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func interact(speaker):
	var dialogue = get_tree().get_first_node_in_group("DIALOGUE")
	dialogue.choice(speaker, question, answers)
	dialogue.answer.connect(_on_answer, CONNECT_ONE_SHOT)
	print("Eai")

func _on_answer(index):
	if index == 0:
		character.state = state_change[0]
	elif index == 1:
		character.state = state_change[1]
	character.state.interact(character)
