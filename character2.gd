extends CharacterBody2D

@export var character_name : String
@export var character_color : Color
@export var character_speech : Array[String] = ["Eaí amigooo!!!", "EI!", "Eaí amigaaa!!!"]
var is_area_entered = false
var is_dialogue_active = false
@export var state : Node


func _physics_process(delta):
	interact_with_character()
	
	
func _on_area_2d_body_entered(body):
	is_area_entered = true
	print("Entrei galera")


func _on_area_2d_body_exited(body):
	is_area_entered = false
	print("Saí galera")
	
	
func interact_with_character():
	if Input.is_action_just_pressed("interact") and is_area_entered and is_dialogue_active == false:
		print(character_speech)
		is_dialogue_active = true
		state.interact(self)
		

